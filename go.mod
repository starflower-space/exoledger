module gitlab.com/starflower-space/exoledger

go 1.17

require gitlab.com/starflower-space/go-cashapp v0.0.0-20211005205151-b61b93fc37e4

require github.com/gocarina/gocsv v0.0.0-20210516172204-ca9e8a8ddea8 // indirect
