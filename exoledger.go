package main

import (
	"fmt"
	"gitlab.com/starflower-space/go-cashapp"
)

func main() {
	transactions, err := cashapp.Parse()
	if err != nil {
		panic(err)
	}

	for _, t := range transactions {
		fmt.Println("Hello", t.Name)
	}
}
